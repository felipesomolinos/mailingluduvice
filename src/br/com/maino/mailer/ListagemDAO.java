package br.com.maino.mailer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;

import org.postgresql.ds.PGSimpleDataSource;

public class ListagemDAO {

	private DataSource dataSource;
	
	public ListagemDAO() throws ClassNotFoundException{
		
		PGSimpleDataSource pgDataSource = new PGSimpleDataSource();
		pgDataSource.setServerName("localhost");
		pgDataSource.setPortNumber(5433);
		pgDataSource.setDatabaseName("Mailer");
		pgDataSource.setUser("postgres");
		pgDataSource.setPassword("postgres");
	
		this.dataSource = pgDataSource;
	}


	public Collection<String> buscaEmailsEnviados() throws SQLException{
		
		Connection conn = this.dataSource.getConnection();
		
		try{
			ArrayList<String> resposta = new ArrayList<String>();
			
			PreparedStatement ps = conn.prepareStatement("select email from envios");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				resposta.add(rs.getString("email"));
			}
			
			rs.close();
			ps.close();
			
			return resposta;
			
		}finally{
			if(conn!=null) conn.close();
		}
	}
	
	public boolean verificaSeEmailJaFoiEnviado(String email) throws SQLException{

		Connection conn = null;
		PreparedStatement ps = null;
		
		try{
		
			conn = this.dataSource.getConnection();
			
			ps = conn.prepareStatement("select 1 from envios where email = ?");
			ps.setString(1, email);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				rs.close();
				return true;
			}
			
			return false;
		
		}finally{
			ps.close();
			conn.close();
		}
		
		
	}
	
	public void gravaEnvio(String nome, String email) throws Exception{
		
		Connection conn = this.dataSource.getConnection();

		try{
			PreparedStatement ps = conn.prepareStatement("insert into ENVIOS (nome, email) values (?, ?)");
			ps.setString(1, nome);
			ps.setString(2, email);
			
			int affected = ps.executeUpdate();
			
			if(affected!=1){
				throw new Exception("Nao foi possivel gravar o dado solicitado no banco. Nome: "+nome+" | Email: "+email);
			}
			
			ps.close();
		
		}finally{
			if(conn!=null) conn.close();
		}
	}
	
	public Collection<RegistroVO> buscaEmailsAEnviar(String nomeArquivo) throws SQLException{
		
		Connection conn = this.dataSource.getConnection();
		
		try{
			PreparedStatement ps = conn.prepareStatement("select nome, email, local from lista where nome_arquivo = ?");
			ps.setString(1, nomeArquivo);
			
			Collection<RegistroVO> registros = new ArrayList<RegistroVO>();
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				RegistroVO registro = new RegistroVO(rs.getString("nome"), rs.getString("email"), rs.getString("local"));
				registros.add(registro);
			}
			
			rs.close();
			ps.close();
			
			return registros;
		
		}finally{
			if(conn!=null) conn.close();
		}
	}
	
	public Collection<RegistroVO> buscaEmailsNaoEnviados() throws SQLException{
		
		Connection conn = this.dataSource.getConnection();
		
		try{

			PreparedStatement ps = conn.prepareStatement("SELECT L.nome, L.email, L.local FROM LISTA L WHERE L.email not in (select e.email from envios e)");

			Collection<RegistroVO> registros = new ArrayList<RegistroVO>();
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				RegistroVO registro = new RegistroVO(rs.getString("nome"), rs.getString("email"), rs.getString("local"));
				registros.add(registro);
			}
			
			rs.close();
			ps.close();
			
			return registros;
			
		} finally {
			if(conn!=null) conn.close();
		}
	}
	
	
}

package br.com.maino.mailer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

public class GeradorDeCargaParaMailchimp {

	public static void main(String[] args) throws Exception{

		ListagemDAO dao = new ListagemDAO();
		
		Collection<RegistroVO> registros = dao.buscaEmailsNaoEnviados();
		
		File arquivo = new File("/home/felipe/Fontes/workspace/Mailer/resource", "BaseParaMailchimp.csv");
		if(arquivo.exists()){
			arquivo.delete();
		}
		arquivo.createNewFile();
		
		Collection<String> linhas = new ArrayList<String>();
		linhas.add("Nome, Email");
		for(RegistroVO registro : registros){
			String linha = registro.getNome()+", "+registro.getEmail();
			linhas.add(linha);
		}
		
		FileUtils.writeLines(arquivo, linhas);
		
		System.out.println("Fim do processamento.");
		
	}

}

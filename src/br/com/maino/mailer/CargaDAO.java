package br.com.maino.mailer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.postgresql.ds.PGSimpleDataSource;

public class CargaDAO {

	private DataSource dataSource;
	
	public CargaDAO() {
		PGSimpleDataSource pgDataSource = new PGSimpleDataSource();
		pgDataSource.setServerName("localhost");
		pgDataSource.setPortNumber(5433);
		pgDataSource.setDatabaseName("Mailer");
		pgDataSource.setUser("postgres");
		pgDataSource.setPassword("postgres");
	
		this.dataSource = pgDataSource;
	}

	public void carregaListagemDeDestinatarios(Collection<RegistroVO> registros, String nomeArquivo) throws Exception{
		
		Connection conn = null;
		
		try{
			
			int contador = 0;
			int contadorDuplicados = 0;
			
			for(RegistroVO registro : registros){
	
				conn = this.dataSource.getConnection();
				
				PreparedStatement ps = conn.prepareStatement("insert into LISTA (email, nome, local, nome_arquivo) values (?, ?, ?, ?)");
				ps.setString(1, StringUtils.trim(registro.getEmail()));
				ps.setString(2, StringUtils.trim(registro.getNome()));
				ps.setString(3, StringUtils.trim(registro.getLocal()));
				ps.setString(4, nomeArquivo);
				
				try{
					int affected = ps.executeUpdate();
					
					if(affected!=1){
						throw new Exception("Nao foi possivel inserir o registro Email:"+registro.getEmail()+" | Nome:"+registro.getNome()+" na tabela LISTA");
					}
				}catch(SQLException ex){
					if(!StringUtils.contains(ex.getMessage(), "ERROR: duplicate key value violates unique constraint \"lista_email_key\"")){
						throw ex;
					}else{
						System.out.println("E-mail duplicado descartado #"+(++contadorDuplicados)+" email: "+StringUtils.trim(registro.getEmail()));
					}
				}
	
				++contador;
				if ((contador%1000)==0) System.out.println("Contador: "+contador);
				
				ps.close();
				conn.close();
			}
			
		}finally{
			if(conn!=null)conn.close();
		}
	}
}

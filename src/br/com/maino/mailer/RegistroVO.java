package br.com.maino.mailer;

public class RegistroVO {

	private String nome;
	private String email;
	private String local;
	
	public RegistroVO(String nome, String email, String local) {
		super();
		this.nome = nome;
		this.email = email;
		this.setLocal(local);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

}

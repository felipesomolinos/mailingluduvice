package br.com.maino.mailer;

import java.util.ArrayList;
import java.util.Collection;


public class Transmissor {

	public static void main(String[] args) {

		try {
			
			String arquivoATransmitir = args[0];
//			String arquivoATransmitir = "PROFISISONAIS-RIO-7.csv";
			
			ListagemDAO dao = new ListagemDAO();
			Collection<RegistroVO> registrosAEnviar = dao.buscaEmailsAEnviar(arquivoATransmitir);
			Collection<RegistroVO> registrosConfirmadosParaEnvio = new ArrayList<RegistroVO>();
			
			for(RegistroVO registro : registrosAEnviar){
				if(!dao.verificaSeEmailJaFoiEnviado(registro.getEmail())){
					registrosConfirmadosParaEnvio.add(registro);
				}
			}
			
			int contador = 0;
			for(RegistroVO registro : registrosConfirmadosParaEnvio){
				Mailer.sendMail(registro.getEmail(), registro.getNome(), registro.getLocal());
				dao.gravaEnvio(registro.getNome(), registro.getEmail());
				++contador;
				if((contador%100)==0) System.out.println(contador);
			}
			
			System.out.println("#FIM de transmissao# -- " + arquivoATransmitir);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

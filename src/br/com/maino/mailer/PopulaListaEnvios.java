package br.com.maino.mailer;

import java.io.File;
import java.util.Collection;

public class PopulaListaEnvios {

	public static void main(String[] args) throws Exception {
		
		File diretorio = new File("/home/felipe/Fontes/workspace/Mailer/resource");

		String[] arquivos = args; 
//			{"engenheiros_por_local_de_votacao.txt"};
		//  {"tecnicos_por_local_de_votacao.txt"};
		
		for(String nomeArquivo : arquivos){
			String separador = "|";
			String encoding = "ISO-8859-15";
			boolean nomePrimeiro = true;
			
			Collection<RegistroVO> registros = CargaDoArquivo.carregarBaseDeEmailsParaEnvio(new File(diretorio, nomeArquivo), separador, encoding, nomePrimeiro);
			
			CargaDAO carga = new CargaDAO();
			carga.carregaListagemDeDestinatarios(registros, nomeArquivo);
		}
	}
}

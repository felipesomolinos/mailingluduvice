package br.com.maino.mailer;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class Mailer {
	
	public static void main(String args[]) throws Exception{
		
		System.out.println("Iniciando envio...");
		
		String nome = "Paulo Granja";
		String destinatario = "felipe@maino.com.br";
		String enderecoVotacao = "Av. Rio Branco, 277, grupo 801 - 8º andar, centro - RJ - Cep: 20040.009";
		
		Mailer.sendMail(destinatario, nome, enderecoVotacao);
		
		System.out.println("Envio finalizado!");
	}

	public static void sendMail(String recipient, String name, String votationAddress) throws EmailException, MalformedURLException{
	
		HtmlEmail email = new HtmlEmail();
		
		email.setHostName("smtp.mandrillapp.com");
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator("sistemasenge@sengerj.org.br", "7cc1u9MetPrY-2RLDgrv2A"));
		email.setSSLOnConnect(true);
		email.setFrom("novo.crea@sengerj.org.br", "Queremos um novo CREA");
		email.addTo(recipient);
		email.setSubject("Eleições Crea/RJ: Senge-RJ informa seu local de votação");
		
		URL url = new URL("https://s3-sa-east-1.amazonaws.com/static.sengerj.org.br/Campanha-CONFEA-CREA-MUTUA.png");
		String cid = email.embed(url, "Eleições Crea/RJ: Senge-RJ informa seu local de votação");
		  
		String textoBoletim = 	"﻿<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
				"<head>" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /> "+
				"<title>Eleições Crea/RJ: Senge-RJ informa seu local de votação</title>"+
				"</head>"+
				""+
				"<body>"+
				"<p>Prezado(a) <font style=\"font-weight: bold;\">"+name+"</font>,</p>"+
				"<p>Lembramos que as eleições para eleger o Presidente do Confea, do Crea/RJ e o Diretor Geral da Mutua ocorrerão no próximo dia 19 de novembro das 9h às 19h.</p>"+
				"<p>Para que possa exercer o seu direito de voto, informamos que o local de votação determinado para você é o seguinte:</p>"+
				"<p style=\"font-weight: bold;\">"+votationAddress+"</p>"+
				"<p>Compareça munido da sua carteira do Crea. O Sistema Cofea/Crea passa pela maior crise de sua história. O presidente do Confea, candidato à reeleição, responde a mais de 30 processos, tendo sido condenado em vários deles e está com seus bens indisponíveis pela Justiça, envergonhando nossos profissionais. Infelizmente, o candidato de situação daqui do RJ, que sempre condenou esta situação, agora, aliou-se a ele.</p>"+
				"<p>Pediram a impugnação de praticamente todas as candidaturas de oposição e aproveitaram para publicar a revista do Crea, apresentando somente o candidato da situação. Precisou que o Judiciário interviesse para garantir a permanência das  candidaturas de oposição. Não bastasse isso, muitos profissionais tiveram seus locais de votação alterados, sem que fosse solicitado, dificultando a participação.</p>"+
				"<p>Diante disso, temos mais motivos ainda para votar. Precisamos mudar. É hora de moralizarmos o Sistema e devolvê-lo aos profissionais.</p>"+
				"<p>Participe e vamos, juntos, tornar o <font style=\"font-weight: bold;\">CREA-RJ MAIS ATUANTE E FORTE POR VOCÊ!</font></p>"+
				"<p><img src=\"cid:"+cid+"\"></p>"+
				"<p style=\"font-weight: bold;\">Saudações</p>"+
				"<p style=\"font-weight: bold;\">A Diretoria</p>"+
				"</body>"+
				"</html>";
		
		
		email.setHtmlMsg(textoBoletim);
		
		email.send();
	}
	
}

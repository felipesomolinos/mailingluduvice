package br.com.maino.mailer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class CargaDoArquivo {

	public static Collection<RegistroVO> carregarBaseDeEmailsParaEnvio(File arquivo, String caracterSeparador, String encoding, boolean nomePrimeiro) throws IOException{
		
		ArrayList<RegistroVO> registros = new ArrayList<RegistroVO>();
		
		List<String> linhas = FileUtils.readLines(arquivo, encoding);
		for (String linha : linhas){
			
			String[] linhaProcessada = StringUtils.split(linha, caracterSeparador);
			if(linhaProcessada.length == 0) continue;
			try{
				RegistroVO registro = nomePrimeiro?new RegistroVO(linhaProcessada[0], linhaProcessada[1], linhaProcessada[2]):new RegistroVO(linhaProcessada[1], linhaProcessada[0], linhaProcessada[2]);
				registros.add(registro);
			} catch (ArrayIndexOutOfBoundsException ex){
				System.out.println("Linha:"+linha);
				System.out.println("---------------");
				ex.printStackTrace();
				throw ex;
			}
		}
		
		return registros;
	}
}
